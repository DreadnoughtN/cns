FROM gcc:latest

WORKDIR /opt

COPY ./main.cpp .

COPY ./queen.cpp .

COPY ./queen.h .

COPY ./Makefile .

CMD ["make compile"]

CMD ["./chess"]